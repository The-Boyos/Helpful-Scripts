#!/bin/bash
echo "This script downloads, builds, and installs a GCC cross compiler toolchain for any supported architecture."

mkdir cross
cd cross

echo "Enter your optimal binutils version number."
read binutilsversion

echo "Enter your optimal GCC version number."
read gccversion

echo "Enter your target architecture."
read targetarch

echo "Setting environment variables"
export PREFIX="$HOME/opt/cross"
export TARGET=$targetarch
export PATH="$PREFIX/bin:$PATH"
echo "Variables set"

echo "Downloading binutils"
wget ftp://ftp.gnu.org/gnu/binutils/binutils-$binutilsversion.tar.gz
echo "Complete"

echo "Unpacking binutils"
tar xvzf binutils-$binutilsversion.tar.gz
echo "Complete"

mkdir build-binutils
cd build-binutils

echo "Configuring binutils"
../binutils-$binutilsversion/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --with-sysroot --disable-werror
echo "Complete"

echo "Beginning compilation: binutils"
make
echo "Operation complete"

echo "Beginning installation: binutils"
sudo make install
echo "Operation complete"

echo "Installation of Binutils is complete"

echo "Downloading GCC"
wget ftp://ftp.gnu.org/gnu/gcc/gcc-$gccversion/gcc-$gccversion.tar.gz
echo "Complete"

echo "Unpacking GCC"
tar xvzf gcc-$gccversion.tar.gz
echo "Complete"

cd gcc-$gccversion
./contrib/download_prerequisites
cd ..

mkdir build-gcc
cd build-gcc

echo "Configuring GCC"
../gcc-$gccversion/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c --without-headers
echo "Complete"

echo "Beginning compilation: all-gcc"
make all-gcc
echo "Operation complete"

echo "Beginning compilation: all-target-libgcc"
make all-target-libgcc
echo "Operation complete"

echo "Beginning installation: gcc"
sudo make install-gcc
echo "Operation complete"

echo "Beginning installation: target-libgcc"
sudo make install-target-libgcc
echo "Operation complete"

echo "Installation of GCC is complete"

echo "Setting permanent variable"
echo export PATH="$HOME/opt/cross/bin:$PATH" >> ~/.profile
echo "Variable set"

cd ..
rm -rf cross

echo "All operations complete, restart your shell to use your new cross compiler!"